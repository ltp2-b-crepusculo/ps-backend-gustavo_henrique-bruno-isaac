package main

import (
	"log"
	"net/http"

	"github.com/rs/cors"
)

func main() {
	connectDB()
	mux := http.NewServeMux()
	mux.HandleFunc("/login", login)
	mux.HandleFunc("/signup", signup)
	mux.HandleFunc("/signupDoctor", signupDoctor)
	mux.HandleFunc("/profile", getProfile)
	mux.HandleFunc("/updateProfile", updateProfile)
	mux.HandleFunc("/profilePicture", serveProfilePicture)
	mux.HandleFunc("/doctors", getDoctors)
	mux.HandleFunc("/unavailableTimes", getUnavailableTimes)
	mux.HandleFunc("/scheduleAppointment", scheduleAppointment)
	mux.HandleFunc("/listAppointments", listAppointments)
	mux.HandleFunc("/doctorAppointments", listDoctorAppointments)
	mux.HandleFunc("/markAsComplete", markAsComplete)
	mux.HandleFunc("/cancelAppointment", cancelAppointment)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:5173"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
	})

	handler := c.Handler(mux)

	log.Fatal(http.ListenAndServe(":8080", handler))
}
