package main

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Estrutura para agendamentos
type Appointment struct {
	ID                primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Date              string             `json:"date"`
	Specialty         string             `json:"specialty"`
	DoctorEmail       string             `json:"doctorEmail"`
	DoctorName        string             `json:"doctorName"`
	DoctorCPF         string             `json:"doctorCPF"`
	DoctorSexo        string             `json:"doctorSexo"`
	DoctorNascimento  string             `json:"doctorNascimento"`
	DoctorNumero      string             `json:"doctorNumero"`
	Notes             string             `json:"notes"`
	PatientEmail      string             `json:"patientEmail"`
	PatientName       string             `json:"patientName"`
	PatientCPF        string             `json:"patientCPF"`
	PatientSexo       string             `json:"patientSexo"`
	PatientNascimento string             `json:"patientNascimento"`
	PatientNumero     string             `json:"patientNumero"`
	Status            string             `json:"status"`
}

// Mapa de especialidades médicas
var specialtiesMap = map[string][]string{
	"Cardiologia":  {"cardiologia", "cardiology"},
	"Dermatologia": {"dermatologia", "dermatology"},
	"Pediatria":    {"pediatria", "pediatrics"},
	"Ortopedia":    {"ortopedia", "orthopedics"},
}

// Função para obter a lista de médicos
func getDoctors(w http.ResponseWriter, r *http.Request) {
	specialty := r.URL.Query().Get("specialty")
	if specialty == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	specialties := specialtiesMap[specialty]
	if specialties == nil {
		specialties = []string{specialty}
	}

	regexSpecialties := make([]bson.M, len(specialties))
	for i, spec := range specialties {
		regexSpecialties[i] = bson.M{"specialty": bson.M{"$regex": spec, "$options": "i"}}
	}

	collection := client.Database("testdb").Collection("users")
	filter := bson.M{"role": "doctor", "$or": regexSpecialties}
	cursor, err := collection.Find(context.TODO(), filter)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.TODO())

	var doctors []Credentials
	if err = cursor.All(context.TODO(), &doctors); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	response := make([]map[string]string, len(doctors))
	for i, doctor := range doctors {
		response[i] = map[string]string{
			"name":  doctor.Name,
			"email": doctor.Email,
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// Função para obter horários indisponíveis de um médico
func getUnavailableTimes(w http.ResponseWriter, r *http.Request) {
	doctorEmail := r.URL.Query().Get("doctorEmail")
	date := r.URL.Query().Get("date")

	if doctorEmail == "" || date == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	collection := client.Database("testdb").Collection("appointments")
	startOfDay, _ := time.Parse("2006-01-02", date)
	endOfDay := startOfDay.Add(24 * time.Hour)

	filter := bson.M{
		"doctoremail": doctorEmail,
		"date":        bson.M{"$gte": startOfDay.Format(time.RFC3339), "$lt": endOfDay.Format(time.RFC3339)},
	}

	cursor, err := collection.Find(context.TODO(), filter)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.TODO())

	var appointments []Appointment
	if err = cursor.All(context.TODO(), &appointments); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	unavailableTimes := make([]string, len(appointments))
	for i, appointment := range appointments {
		unavailableTimes[i] = appointment.Date
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(unavailableTimes)
}

// Função para agendar um compromisso
func scheduleAppointment(w http.ResponseWriter, r *http.Request) {
	tokenStr := r.Header.Get("Authorization")[7:]
	if tokenStr == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil || !token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	var appointment Appointment
	err = json.NewDecoder(r.Body).Decode(&appointment)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	appointment.PatientEmail = claims.Email
	appointment.Status = "pending"

	// Obter informações do paciente
	collection := client.Database("testdb").Collection("users")
	var patient Credentials
	err = collection.FindOne(context.TODO(), bson.M{"email": appointment.PatientEmail}).Decode(&patient)
	if err == nil {
		appointment.PatientName = patient.Name
		appointment.PatientCPF = patient.CPF
		appointment.PatientSexo = patient.Sexo
		appointment.PatientNascimento = patient.Nascimento
		appointment.PatientNumero = patient.Numero
	}

	// Obter informações do médico
	err = collection.FindOne(context.TODO(), bson.M{"email": appointment.DoctorEmail}).Decode(&patient)
	if err == nil {
		appointment.DoctorName = patient.Name
		appointment.DoctorCPF = patient.CPF
		appointment.DoctorSexo = patient.Sexo
		appointment.DoctorNascimento = patient.Nascimento
		appointment.DoctorNumero = patient.Numero
	}

	collection = client.Database("testdb").Collection("appointments")

	// Verificar se já existe um agendamento para o mesmo médico no mesmo horário
	filter := bson.M{
		"doctoremail": appointment.DoctorEmail,
		"date":        appointment.Date,
	}
	count, err := collection.CountDocuments(context.TODO(), filter)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if count > 0 {
		w.WriteHeader(http.StatusConflict)
		return
	}

	_, err = collection.InsertOne(context.TODO(), appointment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

// Função para listar compromissos de um paciente
func listAppointments(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	if email == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	collection := client.Database("testdb").Collection("appointments")
	filter := bson.M{"patientemail": email}

	cursor, err := collection.Find(context.TODO(), filter)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.TODO())

	var appointments []Appointment
	if err = cursor.All(context.TODO(), &appointments); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Buscar nomes dos médicos para cada compromisso
	userCollection := client.Database("testdb").Collection("users")
	for i, appointment := range appointments {
		var doctor Credentials
		err = userCollection.FindOne(context.TODO(), bson.M{"email": appointment.DoctorEmail}).Decode(&doctor)
		if err == nil {
			appointments[i].DoctorName = doctor.Name // Adicionar nome do médico
			appointments[i].DoctorCPF = doctor.CPF
			appointments[i].DoctorSexo = doctor.Sexo
			appointments[i].DoctorNascimento = doctor.Nascimento
			appointments[i].DoctorNumero = doctor.Numero
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(appointments)
}

// Função para listar compromissos de um médico
func listDoctorAppointments(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	if email == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	collection := client.Database("testdb").Collection("appointments")
	filter := bson.M{"doctoremail": email}

	cursor, err := collection.Find(context.TODO(), filter)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer cursor.Close(context.TODO())

	var appointments []Appointment
	if err = cursor.All(context.TODO(), &appointments); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Buscar nomes dos pacientes para cada compromisso
	userCollection := client.Database("testdb").Collection("users")
	for i, appointment := range appointments {
		var patient Credentials
		err = userCollection.FindOne(context.TODO(), bson.M{"email": appointment.PatientEmail}).Decode(&patient)
		if err == nil {
			appointments[i].PatientName = patient.Name // Adicionar nome do paciente
			appointments[i].PatientCPF = patient.CPF
			appointments[i].PatientSexo = patient.Sexo
			appointments[i].PatientNascimento = patient.Nascimento
			appointments[i].PatientNumero = patient.Numero
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(appointments)
}

// Função para marcar um compromisso como concluído
func markAsComplete(w http.ResponseWriter, r *http.Request) {
	var data struct {
		ID string `json:"id"`
	}

	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	collection := client.Database("testdb").Collection("appointments")
	objectID, err := primitive.ObjectIDFromHex(data.ID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	filter := bson.M{"_id": objectID}
	update := bson.M{"$set": bson.M{"status": "completed"}}

	_, err = collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Função para cancelar um compromisso
func cancelAppointment(w http.ResponseWriter, r *http.Request) {
	var data struct {
		ID string `json:"id"`
	}

	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	collection := client.Database("testdb").Collection("appointments")
	objectID, err := primitive.ObjectIDFromHex(data.ID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	filter := bson.M{"_id": objectID}
	update := bson.M{"$set": bson.M{"status": "cancelled"}}

	_, err = collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
