package main

import (
	"context"
	"encoding/json"
	"io"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
)

// Estrutura para credenciais de usuário
type Credentials struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	Role           string `json:"role"`
	Name           string `json:"name"`
	CPF            string `json:"cpf"`
	Sexo           string `json:"sexo"`
	Nascimento     string `json:"nascimento"`
	Numero         string `json:"numero"`
	ProfilePicture string `json:"profilePicture"`
	Specialty      string `json:"specialty"`
}

// Função para registro de usuário
func signup(w http.ResponseWriter, r *http.Request) {
	var creds Credentials
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if !validateEmail(creds.Email) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Email inválido"))
		return
	}

	if !validateCPF(creds.CPF) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("CPF inválido"))
		return
	}

	if !validateNascimento(creds.Nascimento) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Data de nascimento inválida"))
		return
	}

	if !validateNumero(creds.Numero) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Número de telefone inválido"))
		return
	}

	if !validatePassword(creds.Password) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Senha inválida. Deve ter no mínimo 8 caracteres e incluir um caractere especial"))
		return
	}

	creds.Role = "patient"

	collection := client.Database("testdb").Collection("users")
	var result Credentials

	err = collection.FindOne(context.TODO(), bson.M{"email": creds.Email}).Decode(&result)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("Email já existe"))
		return
	}

	err = collection.FindOne(context.TODO(), bson.M{"cpf": creds.CPF}).Decode(&result)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("CPF já existe"))
		return
	}

	err = collection.FindOne(context.TODO(), bson.M{"numero": creds.Numero}).Decode(&result)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("Número de telefone já existe"))
		return
	}

	hashedPassword, err := hashPassword(creds.Password)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	creds.Password = hashedPassword

	_, err = collection.InsertOne(context.TODO(), creds)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

// Função para registro de médico
func signupDoctor(w http.ResponseWriter, r *http.Request) {
	adminEmail := r.Header.Get("admin-email")
	if adminEmail == "" {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Somente administradores podem adicionar médicos"))
		return
	}

	if !isAdmin(adminEmail) {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Somente administradores podem adicionar médicos"))
		return
	}

	var creds Credentials
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if !validateEmail(creds.Email) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Email inválido"))
		return
	}

	if !validateCPF(creds.CPF) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("CPF inválido"))
		return
	}

	if !validateNascimento(creds.Nascimento) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Data de nascimento inválida"))
		return
	}

	if !validateNumero(creds.Numero) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Número de telefone inválido"))
		return
	}

	if !validatePassword(creds.Password) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Senha inválida. Deve ter no mínimo 8 caracteres e incluir um caractere especial"))
		return
	}

	creds.Role = "doctor"

	collection := client.Database("testdb").Collection("users")
	var result Credentials

	err = collection.FindOne(context.TODO(), bson.M{"email": creds.Email}).Decode(&result)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("Email já existe"))
		return
	}

	err = collection.FindOne(context.TODO(), bson.M{"cpf": creds.CPF}).Decode(&result)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("CPF já existe"))
		return
	}

	err = collection.FindOne(context.TODO(), bson.M{"numero": creds.Numero}).Decode(&result)
	if err == nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte("Número de telefone já existe"))
		return
	}

	hashedPassword, err := hashPassword(creds.Password)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	creds.Password = hashedPassword

	_, err = collection.InsertOne(context.TODO(), creds)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

// Função para verificar se o usuário é administrador
func isAdmin(email string) bool {
	collection := client.Database("testdb").Collection("users")
	var result Credentials
	err := collection.FindOne(context.TODO(), bson.M{"email": email, "role": "admin"}).Decode(&result)
	return err == nil
}

// Função para obter o perfil do usuário
func getProfile(w http.ResponseWriter, r *http.Request) {
	tokenStr := r.Header.Get("Authorization")[7:]
	if tokenStr == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil || !token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	collection := client.Database("testdb").Collection("users")
	var result Credentials
	err = collection.FindOne(context.TODO(), bson.M{"email": claims.Email}).Decode(&result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	profile := map[string]string{
		"email":          result.Email,
		"name":           result.Name,
		"profilePicture": result.ProfilePicture,
	}

	json.NewEncoder(w).Encode(profile)
}

// Função para atualizar o perfil do usuário
func updateProfile(w http.ResponseWriter, r *http.Request) {
	tokenStr := r.Header.Get("Authorization")[7:]
	if tokenStr == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil || !token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	err = r.ParseMultipartForm(10 << 20) // 10 MB
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	updateFields := bson.M{}

	newEmail := r.FormValue("email")
	if newEmail != "" && validateEmail(newEmail) {
		updateFields["email"] = newEmail
	}

	name := r.FormValue("name")
	if name != "" {
		updateFields["name"] = name
	}

	cpf := r.FormValue("cpf")
	if cpf != "" && validateCPF(cpf) {
		updateFields["cpf"] = cpf
	}

	sexo := r.FormValue("sexo")
	if sexo != "" {
		updateFields["sexo"] = sexo
	}

	nascimento := r.FormValue("nascimento")
	if nascimento != "" && validateNascimento(nascimento) {
		updateFields["nascimento"] = nascimento
	}

	numero := r.FormValue("numero")
	if numero != "" && validateNumero(numero) {
		updateFields["numero"] = numero
	}

	password := r.FormValue("password")
	if password != "" && validatePassword(password) {
		hashedPassword, err := hashPassword(password)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		updateFields["password"] = hashedPassword
	}

	file, header, err := r.FormFile("profilePicture")
	if err == nil {
		defer file.Close()
		bucket, _ := gridfs.NewBucket(
			client.Database("testdb"),
		)
		uploadStream, err := bucket.OpenUploadStream(header.Filename)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		defer uploadStream.Close()
		_, err = io.Copy(uploadStream, file)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		updateFields["profilePicture"] = header.Filename
	}

	if len(updateFields) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	collection := client.Database("testdb").Collection("users")
	update := bson.M{"$set": updateFields}

	var result Credentials

	// Verificar a unicidade de email, cpf e numero
	if newEmail != "" {
		err = collection.FindOne(context.TODO(), bson.M{"email": newEmail}).Decode(&result)
		if err == nil {
			w.WriteHeader(http.StatusConflict)
			w.Write([]byte("Email já existe"))
			return
		}
	}

	if cpf != "" {
		err = collection.FindOne(context.TODO(), bson.M{"cpf": cpf}).Decode(&result)
		if err == nil {
			w.WriteHeader(http.StatusConflict)
			w.Write([]byte("CPF já existe"))
			return
		}
	}

	if numero != "" {
		err = collection.FindOne(context.TODO(), bson.M{"numero": numero}).Decode(&result)
		if err == nil {
			w.WriteHeader(http.StatusConflict)
			w.Write([]byte("Número de telefone já existe"))
			return
		}
	}

	_, err = collection.UpdateOne(context.TODO(), bson.M{"email": claims.Email}, update)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Função para servir a imagem de perfil
func serveProfilePicture(w http.ResponseWriter, r *http.Request) {
	pictureID := r.URL.Query().Get("id")
	if pictureID == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	bucket, _ := gridfs.NewBucket(
		client.Database("testdb"),
	)

	downloadStream, err := bucket.OpenDownloadStreamByName(pictureID)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	defer downloadStream.Close()

	_, err = io.Copy(w, downloadStream)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
