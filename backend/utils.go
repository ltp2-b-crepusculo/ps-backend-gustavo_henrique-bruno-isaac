package main

import (
	"regexp"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// Função para hashear a senha
func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// Função para verificar se a senha hash corresponde à senha fornecida
func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// Função para validar e-mail
func validateEmail(email string) bool {
	re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$`)
	return re.MatchString(email)
}

// Função para validar CPF
func validateCPF(cpf string) bool {
	re := regexp.MustCompile(`^\d{3}\.\d{3}\.\d{3}\-\d{2}$`)
	return re.MatchString(cpf)
}

// Função para validar data de nascimento
func validateNascimento(nascimento string) bool {
	date, err := time.Parse("2006-01-02", nascimento)
	if err != nil {
		return false
	}
	return date.Before(time.Now())
}

// Função para validar número de telefone
func validateNumero(numero string) bool {
	re := regexp.MustCompile(`^\(\d{2}\)\d{4,5}\-\d{4}$`)
	return re.MatchString(numero)
}

// Função para validar senha
func validatePassword(password string) bool {
	if len(password) < 8 {
		return false
	}

	hasLetter := false
	hasSpecial := false
	specialChars := "!@#$%^&*"

	for _, char := range password {
		switch {
		case char >= 'a' && char <= 'z':
			hasLetter = true
		case char >= 'A' && char <= 'Z':
			hasLetter = true
		case strings.ContainsRune(specialChars, char):
			hasSpecial = true
		}
	}

	return hasLetter && hasSpecial
}
